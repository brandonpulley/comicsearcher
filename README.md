## Setup
This app uses the marvel.com REST api, which requires a public and private key to use.

You can request a key from [developer.marvel.com](https://developer.marvel.com/).

Add your api keys obtains from the developer portal into the `local.properties` file of the root directory. If this file doesn't exist, create it and add the following:
```shell script
marvel_dev_private_key=API PRIVATE KEY FROM STEP ABOVE
marvel_dev_public_key=API PUBLIC KEY FROM STEP ABOVE
```

To verify that your keys have been correctly added you can run the `com.ten.mtodplay.api.OneAppApiTest` file in Android Studio to see the results

After verifying the api you should be able to deploy the `app` module to your device.

Further verify both tests in the UI automated `OneAppApiTest` located in the `app` module

### Third-party Libraries used
- [Glide](https://bumptech.github.io/glide/)
- [Retrofit](https://square.github.io/retrofit/)
- [OkHttp](https://square.github.io/okhttp/)