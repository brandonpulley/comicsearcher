package com.example.api

import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.jupiter.api.Test

class ApiTest {

    companion object {
        val interceptor: RestApi.MarvelAuthInterceptor = RestApi.MarvelAuthInterceptor(
            System.getProperty("marvel_dev_private_key") ?: "",
            System.getProperty("marvel_dev_public_key") ?: ""
        )
    }

    private fun getMarvelApiInterface(): RestApi.MarvelInterface = RestApi.createMarvelInterface(interceptor)

    @Test
    fun getComics() {
        runBlocking {
            val response = getMarvelApiInterface().listComics()
            assertTrue(response.isSuccessful)
            //simple check to make sure that at least one "Comic" is being returned
            assertTrue(response.body()?.data?.results?.size ?: 0 > 0)
        }
    }

    @Test
    fun getTestComic() {
        runBlocking {
            val testId = "291"
            val expectedTitle = "Ant-Man (2003) #1"
            val response = getMarvelApiInterface().listComic(testId)
            assertTrue(response.isSuccessful)
            assertEquals(expectedTitle, response.body()?.data?.results?.first()?.title)
        }
    }

}