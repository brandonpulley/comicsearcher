package com.example.api

import com.example.api.model.ComicDataWrapper
import com.google.gson.GsonBuilder
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import java.math.BigInteger
import java.security.MessageDigest

object RestApi {

    private fun md5(input:String): String {
        val md = MessageDigest.getInstance("MD5")
        return BigInteger(1, md.digest(input.toByteArray())).toString(16).padStart(32, '0')
    }

    private fun log(message: String) {
        val now = System.currentTimeMillis()
        println("$now : $message")
    }

    class MarvelAuthInterceptor(private val privateKey: String,
                                private val publicKey: String): Interceptor{
        override fun intercept(chain: Interceptor.Chain): okhttp3.Response {
            val nowTimestampSt = System.currentTimeMillis().toString()
            val newUrl = "${chain.request().url}?ts=$nowTimestampSt&apikey=$publicKey&hash=${md5(
                nowTimestampSt + privateKey + publicKey
            )}"
            val updatedRequest = chain.request().newBuilder()
            return chain.proceed(updatedRequest.url(newUrl).build())
        }

    }

    interface MarvelInterface {
        @GET("v1/public/comics")
        suspend fun listComics(): Response<ComicDataWrapper>

        @GET("v1/public/comics/{id}")
        suspend fun listComic(@Path("id") comicId: String): Response<ComicDataWrapper>
    }

    fun createMarvelInterface(marvelAuthInterceptor: MarvelAuthInterceptor): MarvelInterface {

        val myHttpLogger = HttpLoggingInterceptor.Logger { message -> log(message) }
        val retrofitLogger = HttpLoggingInterceptor(myHttpLogger).apply {
            this.level = HttpLoggingInterceptor.Level.BODY // log the HTTP response body
        }

        val okHttpClientBuilder = OkHttpClient.Builder()
            .addInterceptor(marvelAuthInterceptor)
            .addInterceptor(retrofitLogger)

        val gson = GsonBuilder()
            .setLenient()
            .create()

        val retrofit = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create(gson))
            .client(okHttpClientBuilder.build())
            .baseUrl("https://gateway.marvel.com")
            .build()

        return retrofit.create(MarvelInterface::class.java)
    }
}