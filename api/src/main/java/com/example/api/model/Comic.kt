package com.example.api.model

data class Comic(
    val title: String,
    val description: String?,
    val thumbnail: Thumbnail?,
    val urls: List<Urls>?
)

data class Urls(
    val type: String,
    val url: String
)

data class Thumbnail(
    val path: String?,
    val extension: String?
)