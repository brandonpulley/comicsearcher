package com.example.api.model

data class ComicDataWrapper(
    val code: Int?,
    val status: String?,
    val data: ComicDataContainer
)
