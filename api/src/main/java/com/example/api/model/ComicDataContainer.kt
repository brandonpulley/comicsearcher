package com.example.api.model

data class ComicDataContainer(
    val results: List<Comic>?
)
