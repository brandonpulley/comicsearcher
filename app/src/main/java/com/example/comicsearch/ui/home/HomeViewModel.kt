package com.example.comicsearch.ui.home

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.api.RestApi
import com.example.comicsearch.BuildConfig
import com.example.comicsearch.R
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class HomeViewModel(private val context: Application) : AndroidViewModel(context) {

    companion object{
        data class Comic(
            val title: String,
            val description: String,
            val coverImage: String,
            val attributionUrl: String
        )
    }

    private val _text = MutableLiveData<String>().apply {
        value = "You've landed on the landing page!"
    }
    private lateinit var _listComics: MutableLiveData<List<Comic>>
    private lateinit var _requestedComic: MutableLiveData<Comic>

    private val restApi = RestApi.createMarvelInterface(
        RestApi.MarvelAuthInterceptor(
            BuildConfig.marvelDevPrivateKey, BuildConfig.marvelDevPublicKey
        )
    )

    val text: LiveData<String> = _text

    val listComics: LiveData<List<Comic>> get() {
        if (!::_listComics.isInitialized) {
            // only grab this list once per app session
            _listComics = MutableLiveData()
            CoroutineScope(Dispatchers.IO).launch {
                val response = restApi.listComics()
                val returnComicsList = mutableListOf<Comic>()
                response.body()?.data?.results?.forEach { result ->
                    returnComicsList.add(buildComic(result))
                }
                _listComics.postValue(returnComicsList)
            }
        }
        return _listComics
    }

    fun getComic(comicId: String): LiveData<Comic> {
        if (!::_requestedComic.isInitialized) {
            // only grab this list once per app session
            _requestedComic = MutableLiveData()
        }

        CoroutineScope(Dispatchers.IO).launch {
            val response = restApi.listComic(comicId)
            response.body()?.data?.results?.first()?.let { apiComic ->
                _requestedComic.postValue(buildComic(apiComic))
            }
        }
        return _requestedComic
    }

    private fun buildComic(apiComic: com.example.api.model.Comic): Comic = Comic(
        title = apiComic.title,
        description = apiComic.description ?: "",
        coverImage = "${apiComic.thumbnail?.path}${if (apiComic.thumbnail?.path?.isNotEmpty()==true) "." else ""}${apiComic.thumbnail?.extension}",
        attributionUrl = apiComic.urls?.firstOrNull()?.url ?: context.resources.getString(R.string.default_linkback_url)
    )
}