package com.example.comicsearch.ui.home

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.example.comicsearch.databinding.FragmentHomeBinding

class HomeFragment : Fragment() {

    private lateinit var homeViewModel: HomeViewModel
    private var _binding: FragmentHomeBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        homeViewModel = ViewModelProvider(this)[HomeViewModel::class.java]

        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        val root: View = binding.root

        val comicIdEditText = binding.comicIdEt
        val requestButton = binding.getComicButton
        val mainImageView = binding.mainImageview
        val titleTextView = binding.titleTv
        val descriptionTextView = binding.descriptionTv
        requestButton.setOnClickListener {
            homeViewModel.getComic(comicIdEditText.text.toString()).observe(viewLifecycleOwner, { comic ->
                Glide
                    .with(requireContext())
                    .load(comic.coverImage.replace("http:", "https:")) // Glide requires https protocol
                    .into(mainImageView)
                titleTextView.text = comic.title
                descriptionTextView.text = comic.description
                mainImageView.isClickable = true
                mainImageView.setOnClickListener {
                    val intent = Intent(Intent.ACTION_VIEW, Uri.parse(comic.attributionUrl))
                    requireContext().startActivity(intent)
                }
            })
        }

        return root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}