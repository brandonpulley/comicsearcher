package com.example.comicsearch

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class MainInstrumentedTest {

    @get:Rule
    var activityRule: ActivityScenarioRule<MainActivity>
            = ActivityScenarioRule(MainActivity::class.java)

    @Test
    fun searchComic() {
        val expectedServerResponseTime = 2000L
        val comicId = "222"
        val expectedTitle = "Marvel Age Spider-Man (2004) #1"
        onView(withId(R.id.comic_id_et))
            .perform(typeText(comicId), closeSoftKeyboard())
        onView(withId(R.id.get_comic_button)).perform(click())
        Thread.sleep(expectedServerResponseTime)
        onView((withId(R.id.title_tv)))
            .check(matches(withText(expectedTitle)))
    }
}